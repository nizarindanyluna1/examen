const llamandoFetch = () => {

    const url = "https://www.thecocktaildb.com/api/json/v1/1/search.php?s=";
    fetch(url)
      .then((response) => response.json())
      .then((data) => mostrarDatos(data))
      .catch((error) => {
        const lblError = document.getElementById("lnlError");
        lblError.innerHTML = "Ocurrió un error: " + error;
      });

};

const mostrarDatos = (data) => {

    let estatus = document.querySelector('input[name="status"]:checked').value;
    let pict = document.getElementById('drinks');

    let count = 0;
  
    pict.innerHTML='';
    for (let drink of data.drinks) {

        if(estatus == drink.strAlcoholic){
            
            let img = document.createElement('img');
            img.src = drink.strDrinkThumb
            let name = document.createElement('p');
            name.innerHTML = drink.strDrink;
            let divDrink = document.createElement('div');

            divDrink.appendChild(img);
            divDrink.appendChild(name);
            pict.append(divDrink);
      
            count += 1;
            
        }
                    
    }

    let cant = document.getElementById('cant_alcohol');
    cant.innerHTML = count;
    console.log(data.drinks)

};

function limpiar(){

    let pict = document.getElementById('drinks');
    let estatus = document.querySelector('input[name="status"]:checked');
    let cant = document.getElementById('cant_alcohol');

    estatus.checked = false;
    cant.innerHTML = '';
    pict.innerHTML='';

}

document.getElementById("btnListar").addEventListener("click", llamandoFetch);

document.getElementById('btnLimpiar').addEventListener("click",limpiar)